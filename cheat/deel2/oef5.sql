create table `tblManufacturers`
(
`Id` smallint not null primary key auto_increment,
`Name` varchar(75) unique
)
auto_increment=101;


create table tblPlaneDefinitions
(
`Id` mediumint unsigned primary key auto_increment ,
`ManufacturerId` smallint not null ,
foreign key (`ManufacturerId`) references `tblManufacturers` (`Id`)
on update restrict
on delete restrict,

`Name` varchar(110) not null ,
`DesignDate` date not null,
unique (`Name`,DesignDate),

`IsMilitary` tinyint(1),
`NumberofEngines` tinyint,
`Weight` decimal (5,2)
)
;


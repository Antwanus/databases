create table tblOpleidingen
(
`Code` varchar(10) primary key,
`Omschrijving` text,
`Duur` year
);

create table tblAfdeling
(
`Nr` int primary key auto_increment,
`Naam` Varchar(100),
`Replicatiecode` Char(36)
)auto_increment = 10;

create table tblMedewerkers
(
`Nr` tinyint primary key auto_increment,
`Naam` varchar(100),
`Adres` varchar(70),
`AfdelingNr` int,
foreign key (`AfdelingNr`) references tblAfdeling(`Nr`)

);

create table tblGevolgdeOpleidingen
(
`MedewerkerNr` tinyint,
foreign key(`MedewerkerNr`) references tblMedewerkers(`Nr`),

`Opleidingscode` varchar(10),
foreign key(`Opleidingscode`) references tblOpleidingen(`Code`),

`Datum` date,

`Voltooid` timestamp
);
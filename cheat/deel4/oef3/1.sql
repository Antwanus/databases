SELECT 
    SPELERSNR,
    CONCAT('+31(0)',
            SUBSTRING(TELEFOON,
                2,
                LENGTH(TELEFOON) - 1)) AS 'INTTELEFOON'
FROM
    spelers;
select 
	SPELERSNR,
		case 
			when day(GEB_DATUM) = day(current_date) and month(GEB_DATUM) = month(current_date)  then 'Jarig'
			else 'niet jarig'
        end as 'FEEST'
from spelers;
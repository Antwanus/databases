select 
	SPELERSNR,
	JAARTOE,
		case 
        when JAARTOE < 1980 then 'Ouderen'
        when JAARTOE >=1980 and JAARTOE < 1983 then 'Jongeren'
        when JAARTOE >= 1983 then 'Kinderen'
        end as 'GROEP'
        from spelers;
	
select SPELERSNR,
	TEAMNR,
    case
    when GEWONNEN > VERLOREN then 'Gewonnen'
    when GEWONNEN < VERLOREN then 'Verloren'
    end as 'RESULTAAT'
    from wedstrijden;
    
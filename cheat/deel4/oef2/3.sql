select 
	SPELERSNR,
	JAARTOE,
		case 
        when JAARTOE < 1980 and BONDSNR is null then 'Ouderen Wedstrijd'
        when JAARTOE < 1980 and BONDSNR is not null then 'Ouderen recreatief'
        when JAARTOE >=1980 and JAARTOE < 1983 then 'Jongeren'
        when JAARTOE >= 1983 then 'Kinderen'
        end as 'GROEP'
        from spelers;
SELECT 
    SPELERSNR,
    (SELECT 
            COUNT(WEDSTRIJDNR)
        FROM
            wedstrijden w
        WHERE
            s.SPELERSNR = w.SPELERSNR) AS 'Aantal'
FROM
    spelers s;
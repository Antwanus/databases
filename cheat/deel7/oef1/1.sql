SELECT 
    (SELECT 
            t.DIVISIE
        FROM
            teams t
        WHERE
            TEAMNR = 1) AS 'Divisie_t1',
    (SELECT 
            t.DIVISIE
        FROM
            teams t
        WHERE
            TEAMNR = 2) AS 'Divisie_t2'
FROM
    teams;

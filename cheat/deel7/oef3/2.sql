SELECT 
    s.SPELERSNR, s.NAAM, s.VOORLETTERS
FROM
    spelers s
WHERE
    s.SPELERSNR = (SELECT 
            t.SPELERSNR
        FROM
            teams t
        WHERE
            TEAMNR = (SELECT 
                    w.TEAMNR
                FROM
                    wedstrijden w
                WHERE
                    w.WEDSTRIJDNR = 2));
SELECT 
    b.SPELERSNR, b.BETALINGSNR
FROM
    boetes b
WHERE
    b.SPELERSNR IN (SELECT 
            t.SPELERSNR
        FROM
            teams t)
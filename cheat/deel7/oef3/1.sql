SELECT 
    s.SPELERSNR, s.NAAM, s.VOORLETTERS
FROM
    spelers s
WHERE
    s.SPELERSNR = (SELECT 
            b.SPELERSNR
        FROM
            boetes b
        WHERE
            BETALINGSNR = 4)
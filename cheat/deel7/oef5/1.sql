SELECT 
    s.SPELERSNR
FROM
    spelers s,
    wedstrijden w
WHERE
    NOT w.GEWONNEN = 3
        AND w.SPELERSNR = s.SPELERSNR;
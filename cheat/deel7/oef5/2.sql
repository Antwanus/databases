SELECT 
    t.TEAMNR, t.DIVISIE
FROM
    teams t,
    wedstrijden w
WHERE
    w.SPELERSNR = 6
        AND NOT t.TEAMNR = w.TEAMNR;
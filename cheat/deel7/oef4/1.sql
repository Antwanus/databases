SELECT 
    s.SPELERSNR, s.NAAM
FROM
    spelers s
WHERE
    s.SPELERSNR IN (SELECT 
            b.SPELERSNR
        FROM
            boetes b
        WHERE
            b.BEDRAG >= 50) ;
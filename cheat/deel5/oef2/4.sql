select SPELERSNR,JAARTOE,GEB_DATUM, (year(current_date) - JAARTOE) as 'Aantal jaren',


case 
when (year(current_date) - (JAARTOE)) >= 45 then '45+'
when (year(current_date) -(JAARTOE)) >= 40 then '40+'
	when (year(current_date) -(JAARTOE)) >= 35 then '35+'
	
	
end as 'Duurtijd'

from spelers
where year(GEB_DATUM) in (1956,1963,1970);

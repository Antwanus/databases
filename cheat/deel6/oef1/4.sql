SELECT w.WEDSTRIJDNR, s.NAAM, s.PLAATS, t.DIVISIE,
	case 
    when w.GEWONNEN < w.VERLOREN then 'verloren'
    else 'gewonnen'
    end as resultaat
   
FROM
    wedstrijden w,
    spelers s,
    teams t
   
    where s.SPELERSNR = w.SPELERSNR and w.WEDSTRIJDNR <= 10
    and t.TEAMNR = w.TEAMNR;
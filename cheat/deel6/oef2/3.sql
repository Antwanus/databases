SELECT 
    CASE
        WHEN INSTR(s.NAAM, 'de') > 0 THEN CONCAT('de ', REPLACE(s.NAAM, ', de', ' '))
        WHEN INSTR(s.NAAM, 'van') > 0 THEN CONCAT('van ', REPLACE(s.NAAM, ', van', ' '))
        ELSE s.NAAM
    END AS 'NAAM',
    w.*,
    NAAM
FROM
    spelers s
        INNER JOIN
    wedstrijden w ON w.SPELERSNR = s.SPELERSNR
        AND w.GEWONNEN > w.VERLOREN
        AND MONTH(s.GEB_DATUM) BETWEEN 8 AND 10;


SELECT 
    s.*,
    b.FUNCTIE,
    CASE
        WHEN b.EIND_DATUM IS NULL THEN 'Actief'
        ELSE 'Niet Actief'
    END AS 'Activiteit'
FROM
    spelers s
        INNER JOIN
    bestuursleden b ON s.SPELERSNR = b.SPELERSNR
    where FUNCTIE = 'Voorzitter' or FUNCTIE = 'Penningmeester';


create table tblLanguages
(
`Id` int primary key,
`Name` varchar(20) charset ascii
);

create table tblCourseDefinitions
(
`Id` mediumint primary key,
`LanguageId` int,
foreign key(`LanguageId`) references tblLanguages (`Id`),

`Name` varchar(100) charset utf8mb4,

`ReplicationKey` char(36)

);
alter table tblboetes add
(
`REDEN` varchar(255)

);

alter table tblboetes add 
column `PAYMENTDATE` date 
after `BEDRAG`;

alter table tblspelers add
(`VOORNAAM` varchar(50)
);

alter table tblspelers
drop `VOORLETTERS`;

alter table tblboetes
change `PAYMENTDATE` `BETALINGSDATUM` date;

alter table tblspelers
change `NAAM` `NAAM` varchar(50) not null;

alter table tblspelers
add constraint
unique key (`NAAM`,`STRAAT`,`HUISNR`,`POSTCODE`);


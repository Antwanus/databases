SELECT 
    NAAM,
    GEB_DATUM,
    (SELECT 
            COUNT(BEDRAG)
        FROM
            boetes b
        WHERE
            b.SPELERSNR = s.SPELERSNR) AS aantal
FROM
    spelers s
ORDER BY aantal DESC , GEB_DATUM ASC
LIMIT 3



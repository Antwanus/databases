SELECT 
    t.TEAMNR, DIVISIE, SUM(GEWONNEN)
FROM
    TEAMS t,
    wedstrijden w
WHERE
    t.TEAMNR = w.TEAMNR
GROUP BY t.TEAMNR
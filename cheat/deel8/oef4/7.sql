SELECT 
    test.TEAMNR, COUNT(test.SPELERSNR) AS aantal_spelers
FROM
    (SELECT 
        w.TEAMNR, w.SPELERSNR
    FROM
        teams t
    INNER JOIN spelers s ON s.SPELERSNR = t.SPELERSNR
        AND PLAATS = 'Den Haag'
    INNER JOIN wedstrijden w ON GEWONNEN > VERLOREN
        AND w.TEAMNR = t.TEAMNR
    GROUP BY w.SPELERSNR) AS test
GROUP BY TEAMNR
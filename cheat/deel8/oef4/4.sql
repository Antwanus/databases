SELECT 
    s.SPELERSNR, s.NAAM, SUM(b.BEDRAG) AS totaal
FROM
    boetes b,
    spelers s
WHERE
    s.SPELERSNR = b.SPELERSNR
GROUP BY b.SPELERSNR

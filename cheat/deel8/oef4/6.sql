SELECT 
    w.SPELERSNR,
    CASE
        WHEN aantal IS NULL THEN 0
        ELSE aantal
    END AS boetes
FROM
    wedstrijden w
        LEFT JOIN
    (SELECT 
        b.spelersnr, COUNT(b.SPELERSNR) AS aantal
    FROM
        boetes b
    GROUP BY b.SPELERSNR) c ON w.SPELERSNR = c.SPELERSNR
GROUP BY w.SPELERSNR;
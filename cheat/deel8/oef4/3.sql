SELECT 
    TEAMNR,
    COUNT(WEDSTRIJDNR) AS wedstrijden,
    sum(GEWONNEN) as gewonnen
FROM
    wedstrijden
WHERE
    TEAMNR = (SELECT 
            TEAMNR
        FROM
            teams
        WHERE
            DIVISIE = 'ere')
GROUP BY TEAMNR

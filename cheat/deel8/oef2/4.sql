SELECT 
    NAAM,
    (SELECT 
            COUNT(BEDRAG)
        FROM
            boetes b
            
        WHERE
            b.SPELERSNR = s.SPELERSNR) AS aantal_boetes
FROM
    spelers s
ORDER BY aantal_boetes desc, naam asc

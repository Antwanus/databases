-- 1. Om de oefeningen in deze bundel te maken werken we verder met de database “Tennis3”.
use tennis3;


-- 2. Schrijf de SQL-instructie die ervoor zorgt dat het bedrag van elke boete verhoogd wordt met 5%.
describe boetes;
update boetes set bedrag = (bedrag * 1.05);
--  Error Code: 1175. You are using safe update mode and you tried to update a table without a WHERE that uses a KEY column.  
--  To disable safe mode, toggle the option in Preferences -> SQL Editor and reconnect.


/* 3. Schijf de SQL-instructie die ervoor zorgt dat het bedrag van de boetes uit het jaar 2020 aangepast wordt naar €120.
	Onderzoek hiervoor hoe je een eenvoudige filterconditie kan samenstellen met behulp van internet!*/
update boetes set bedrag = 120 where year(datum) = 2020;


-- 1. Om de oefeningen in deze bundel te maken werken we verder met de database “Tennis3”.
use tennis3;


-- 2. Schrijf de SQL-instructie die ervoor zorgt dat alle records uit tabel “BESTUURSLEDEN” worden verwijderd.alter
truncate table BESTUURSLEDEN;
delete from BESTUURSLEDEN where betalingsnr > 0;


-- 3. Schijf de SQL-instructie die ervoor zorgt dat de boetes van speler 7 worden verwijderd.
delete from boetes where spelersnr = 7;
-- Onderzoek hiervoor (indien nodig) hoe je een eenvoudige filterconditie kan samenstellen met behulp van internet!
/* 1. Om de oefeningen in deze bundel te maken gaan we een duplicaat maken van de database “Tennis”.
a. Kopieer de inhoud van het SQL-script van de database “Tennis”.
b. Plak de inhoud in een leeg query editor venster.
c. Wijzig de naam “Tennis” in “Tennis2”.
d. Voer het SQL-script uit om het duplicaat aan te maken.
Gebruik deze database “Tennis2” bij de volgende oefeningen in deze oefenbundel. */
use tennis2;


-- 2. Schrijf de SQL-instructies die ervoor zorgen dat alle tabellen in de database “Tennis2” de prefix “tbl” krijgen.
rename table bestuursleden to tbl_bestuursleden;
rename table boetes to tbl_boetes;
rename table spelers to tbl_spelers;
rename table teams to tbl_teams;
rename table wedstrijden to tbl_wedstrijden;


-- 3. Schrijf de SQL-instructie die in tabel “tblBoetes” een kolom “REDEN” toevoegt. Deze kolom heeft als datatype VARCHAR(255)
-- alter table tbl_boetes drop column reden;
alter table tbl_boetes add (
	REDEN varchar(255)
);


-- 4. Schrijf de SQL-instructie die in tabel “tblSpelers” een kolom “VOORNAAM” toevoegt. Deze kolom heeft als datatype VARCHAR(50).
-- alter table tbl_spelers drop column voornaam;
alter table tbl_spelers add 
	column VOORNAAM varchar(50)
;


/* 5. Schrijf de SQL-instructie die in tabel “tblBoetes” een kolom “PAYMENTDATE” toevoegt. Je gebruikt hiervoor het datatype DATE.
Opgelet! Deze kolom moet toegevoegd worden tussen kolommen “BEDRAG” en de hiervoor aangemaakte kolom “REDEN”.
Zoek op internet op hoe je dit kan doen in MySQL. */
alter table tbl_boetes add 
	column PAYMENTDATE date 
		after bedrag;


-- 6. Schrijf de SQL-instructie die in tabel “tblSpelers” de kolom “VOORLETTERS” zal verwijderen.
alter table tbl_spelers drop column VOORLETTERS;


-- 7. Schrijf de SQL-instructie die in tabel “tblBoetes” de naam van kolom “PAYMENTDATE” wijzigt in “BETALINGSDATUM”.
alter table tbl_boetes rename column PAYMENTDATE to BETALINGSDATUM;


-- 8. Schrijf de SQL-instructie die in tabel “tblSpelers” het datatype van de kolom “NAAM” wijzigt naar VARCHAR(50). Daarnaast mag deze kolom geen NULL toelaten.
alter table tbl_spelers change NAAM NAAM varchar(50) not null;


-- 9. Schrijf de SQL-instructie die in tabel “tblSpelers” ervoor zorgt dat er een alternatieve sleutel wordt gelegd op de combinatie van 
--    kolommen “NAAM”,“STRAAT”,”HUISNR” en “POSTCODE”.
alter table tbl_spelers add constraint AK_combo unique(NAAM, STRAAT, HUISNR, POSTCODE);


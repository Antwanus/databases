create database if not exists learning character set utf8mb4;

create table learning.tbl_languages (
	id tinyint unsigned primary key,
    language_name varchar(33) character set latin1
);

create table learning.tbl_course_definitions (
	id tinyint unsigned primary key,
    language_id tinyint unsigned,
    course_name varchar(100) char set utf8mb4,
    replication_key char(16),
    
    foreign key (language_id) references tbl_languages(id)
) default charset ascii;
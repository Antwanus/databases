# 1. Schrijf de SQL-instructie die ervoor zorgt dat een database met naam “Sales” aangemaakt wordt.
CREATE DATABASE IF NOT EXISTS `sales`;

# 2. Schrijf de SQL-instructie die ervoor zorgt dat een database met naam “BachelorIT” aangemaakt wordt. Het aanmaken mag enkel gebeuren indien deze database nog niet bestaat!
CREATE DATABASE IF NOT EXISTS `bachelor_it`;

# 3. Schrijf de SQL-instructie die ervoor zorgt dat de zonet aangemaakte database “BachelorIT” wordt verwijderd.
DROP DATABASE IF EXISTS `bachelor_it`;

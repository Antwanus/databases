1. We bewaren in tabel “tblGameGenres” alle gekende genres. Elk genre krijgt een uniek volgnummer, startend vanaf 1, die bewaard wordt in kolom “Id”.
Er zijn een 60-tal genres gedefinieerd.
Welk datatype zal je gebruiken voor deze kolom? Motiveer je antwoord!

	TINYINT UNSIGNED
	
	tinyint => (byte) 128 + 127 => 0 - 255

_____________________________________________________________________________________________
2. Op een weerbericht wordt de temperatuur getoond. We bewaren deze waarden in kolom “Temperature” van tabel “tblTemperatureHistory”.
Welk datatype zal je gebruiken voor deze kolom? Motiveer je antwoord!

	TINYINT

	-128 .. 127 (er vanuit gaande dat temperatuur in Celsius zal bewaard worden)


_____________________________________________________________________________________________
3. Jaarlijks wordt het aantal inwoners van België geteld. Dit aantal wordt bewaard in kolom “InhabitantCount” van tabel “tblDemoGraphicData”.
Welk datatype zal je gebruiken voor deze kolom? Motiveer je antwoord!

	MEDIUMINT UNSIGNED

	0.. +/- 16 miljoen en wisselgeld	=> migrate over 50 jaar ?

	INT UNSIGNED

	0.. +- 4.3 miljard			=> overkill...






_____________________________________________________________________________________________
4. Veel mensen bestellen jaarlijks artikelen via Amazon. Een bestelling heeft daarbij minimaal één, mogelijk meerdere detaillijnen per artikel. Elke detaillijn van een bestelling krijgt een uniek volgnummer toegewezen.
Welk datatype zal je gebruiken voor dit volgnummer? Motiveer je antwoord!

	GUID?


	Wat is een detaillijn van een artikel? Afgaande van deze info kan ik geen geïnformeerde beslissing maken.. Krijgt elke detaillijn van elk product op elke bestelling een record in de database? In dat geval denk ik aan een soort GUID van datatype VARCHAR, maar dat is niet efficient als Primary Key (PK).
	

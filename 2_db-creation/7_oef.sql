CREATE DATABASE IF NOT EXISTS planes CHARACTER SET latin1;

CREATE TABLE IF NOT EXISTS planes.tbl_manufactorers (
    id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    m_name VARCHAR(70) UNIQUE
) AUTO_INCREMENT=101;

create table if not exists planes.tbl_plane_definitions (
	id int unsigned primary key auto_increment,
    manufacturer_id smallint unsigned,
    p_name varchar(100) not null,
    design_date date not null,
    is_military tinyint(1),
    number_of_engines tinyint unsigned,
    weight dec(5, 2) unsigned,
    
    unique (p_name, design_date),
    foreign key (manufacturer_id) references planes.tbl_manufactorers(id) on delete restrict
);
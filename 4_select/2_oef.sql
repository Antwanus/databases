/* 1. Schrijf de SQL-instructie die in de volgende informatiebehoefte voorziet.
	Toon voor elke wedstrijd de spelersnummer, de teamnummer en de vermelding van het resultaat. 
	Het resultaat toont de waarde “Gewonnen” als het aantal gewonnen sets groter is dan het aantal verloren sets is. 
    Het resultaat toont de waarde “Verloren” als het aantal verloren sets groter is dan het aantal gewonnen sets. 
    Een draw is niet mogelijk in een tenniswedstrijd. Gebruik kolomhoofding “RESULTAAT” voor deze kolom. */
use tennis;
show tables;
describe wedstrijden;
select * from wedstrijden;

select spelersnr, teamnr, gewonnen - verloren,
case
	when gewonnen - verloren  >0 then 'gewonnen'
    when gewonnen - verloren  <0 then 'verloren'
    else 'wtf'
	end as resultaat
from wedstrijden;


/* 2. Schrijf de SQL-instructie die in de volgende informatiebehoefte voorziet.
Toon voor elke speler de spelersnummer, het jaar van toetreding en de vermelding tot welke groep de speler behoort.
a. Een speler behoort tot de groep ‘Ouderen’ als hij/zij voor 1980 is toegetreden.
b. Een speler behoort tot de groep ‘Jongeren’ als hij/zij vanaf 1980 en voor 1983 is
toegetreden.
c. Een speler behoort tot de groep ‘Kinderen’ als hij/zij vanaf 1983 of later is
toegetreden.
Gebruik kolomhoofding “GROEP” voor deze kolom. */
select * from spelers;
select spelersnr, jaartoe,
case
	when jaartoe < 1980 then 'Ouderen'
    when jaartoe < 1983 then 'Jongeren'
    else 'Kinderen'
	end as resultaat
from spelers;

/*3. Schrijf de SQL-instructie die in de volgende informatiebehoefte voorziet.
	Verfijn oefening 2 zodat de groep ‘Ouderen‘ nog verder wordt onderverdeeld in ‘Ouderen wedstrijd’ en ‘Ouderen recreatief’.
	‘Ouderen wedstrijd’ is van toepassing als de speler een bondsnummer heeft.
	HINT: gebruik IS NULL om te controleren of een bondsnummer ontbreekt.*/
select spelersnr, jaartoe,
case
	when jaartoe < 1980 then 
		case
			when bondsnr is null then 'Ouderen Wedstrijd'
            else 'Ouderen Recreatief'
		end
    when jaartoe < 1983 then 'Jongeren'
    else 'Kinderen'
	end as resultaat
from spelers;
use tennis;
show tables;

-- 1. Schrijf de SQL-instructie die in de volgende informatiebehoefte voorziet.
-- Toon voor elke boete het betalingsnummer en het boete bedrag.
describe boetes;
select betalingsnr, bedrag from boetes;

-- 2. Schrijf de SQL-instructie die in de volgende informatiebehoefte voorziet.
-- Toon voor elke wedstrijd de spelersnummer, de teamnummer en het verschil tussen de gewonnen- en verloren sets.
describe wedstrijden;
select spelersnr, teamnr, gewonnen - verloren as num_gewonnen from wedstrijden;

-- 3. Schrijf de SQL-instructie die in de volgende informatiebehoefte voorziet.
-- Toon voor elke wedstrijd de spelersnummer, de teamnummer en het verschil tussen de gewonnen- en verloren sets. 
-- Zorg ervoor dat de kolomnaam van deze bewerking RESULTAAT is.
select spelersnr, teamnr, gewonnen - verloren as RESULTAAT from wedstrijden; 